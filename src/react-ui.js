class NumberInput extends React.Component{
  constructor(){
    super();
    this.state = {
      value: 20
    }
    this.handleChange = this.handleChange.bind(this)
    this.componentDidMount=this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.setState({value: this.props.defaultVal})
  }
  handleChange(e){
    this.setState({value: e.target.value});
  }
  render(){
    return(
<input className="numberValue" onChange={this.handleChange} value={this.state.value} type="number" className="sans customNumber" id={this.props.id}/>
    )
  }
}

class Slider extends React.Component{
  constructor(){
    super();
    this.state ={

    }
    this.handleChange = this.handleChange.bind(this)  
    this.componentDidMount= this.componentDidMount.bind(this)
  }
  componentDidMount(){
    this.setState({value:this.props.defaultVal})
  }
  handleChange(e){
    this.setState({value: e.target.value})
    this.props.value({value:e.target.value,id:e.target.id})
  }
  render(){
    return(
<div>
  <input value={this.state.value} min="0" max="10" onChange={this.handleChange} type="range" className="custom-range" id={this.props.id}></input>         
</div>
    )
  }
}

class SettingsRowNumber extends React.Component{
  render(){
    return(
<div className="row d-flex">
  <div className="six columns d-flex">
      <span className="sans">{this.props.text}</span>
  </div>
  <div className="six columns d-flex">
    <NumberInput id={this.props.id}/>
  </div>
</div>      
    )
  }
}

class FlowRateSettings extends React.Component{
  render(){
    return(
<table>
  <tr>
    <td><div className="content"></div></td>
    <td className='road' style={{borderLeft:'solid 2px #1a1a1a',borderRight:'solid 2px #1a1a1a'}}>
      <div className="content">
        <div style={{margin: 'auto'}}>
            <NumberInput defaultVal='600' id="f1"/>
        </div>    
      </div>
    </td>
    <td></td>
  </tr>
  <tr>
    <td className='road' style={{borderTop:'solid 2px #1a1a1a',borderBottom:'solid 2px #1a1a1a'}}>
      <div className="content">
        <div style={{margin: 'auto'}}>
            <NumberInput defaultVal='600' id="f2"/>
        </div>    
      </div>
    </td>
    <td className='road'></td>
    <td className='road' style={{borderTop:'solid 2px #1a1a1a',borderBottom:'solid 2px #1a1a1a'}}>
      <div className="content">
        <div style={{margin: 'auto'}}>
            <NumberInput defaultVal='600' id="f3"/>
        </div>    
      </div>
    </td>
  </tr>
  <tr>
    <td></td>
    <td className='road' style={{borderLeft:'solid 2px #1a1a1a',borderRight:'solid 2px #1a1a1a'}}>
      <div className="content">
        <div style={{margin: 'auto'}}>
            <NumberInput defaultVal='600' id="f4"/>
        </div>    
      </div>
    </td>
    <td></td>
  </tr>
</table>    
    )
  }
}

class SimulateButton extends React.Component {
  componentDidMount(){
    mdc.ripple.MDCRipple.attachTo(document.querySelector('.mdc-button'));
  }
  render(){
    return(
<button style={{fontFamily: 'Poppins',fontWeight: "600",margin:"auto",marginTop: "1em",fontSize:"13px"}} id="start"
className="mdc-button mdc-button--unelevated">Simulate</button>
    )
  }
}

class Vehicles extends React.Component{
  constructor(){
    super()
    this.state={
      sedan:3,
      truck:2,
      cargo:1
    }
    this.componentDidMount = this.componentDidMount.bind(this)
    this.updateVal = this.updateVal.bind(this)
  }
  componentDidMount(){

  }

  updateVal(val){
    this.setState({[val.id]:val.value})
  }
  render(){
    return(
<div>      
<div className="row mb-3">
  <div className="col-4">
      <span className="sans">Sedan </span><span className="ml-2 sans bold">{this.state.sedan}</span>
  </div>
  <div className="col-8">
    <Slider defaultVal={this.state.sedan} value={this.updateVal} id="sedan"/>
  </div>
</div>  
<div className="row mb-3">
  <div className="col-4">
  <span className="sans">Truck </span><span className="ml-2 sans bold">{this.state.truck}</span>
  </div>
  <div className="col-8">
  <Slider defaultVal={this.state.truck} value={this.updateVal} id="truck"/>
  </div>
</div>
<div className="row">
  <div className="col-4">
  <span className="sans">Cargo </span><span className="ml-2 sans bold">{this.state.cargo}</span>
  </div>
  <div className="col-8">
  <Slider defaultVal={this.state.cargo} value={this.updateVal} id="cargo"/>
  </div>    
</div>

</div>   
   
    )
  }
}


class Settings extends React.Component {
  render() {
    return (
<div className="rounded card-custom pt-3 mb-3">
  <div className="option">
    <span className="sans bold" style={{margin:'auto'}}>Settings</span>
  </div>
  <div className="pl-3 pr-3 pb-3 container splitBottom">
      <div className="row d-flex mt-3">
        <div className="col-12 mb-1">
          <span className="sans bold">Traffic Light</span>
        </div>
        <div className="col-6">
          <span className="sans">Maximum Cycle Length Allowed (sec)</span>
        </div>
        <div className="col-6 mb-3">
          <div className="d-flex">
            <NumberInput defaultVal='40' id="gl"/>
          </div>
        </div>       
      </div>
  </div>
  <div className="container p-3 splitBottom">
      <div className="row">
        <div className="col-12">
          <span className="sans bold">Flow rate<br/></span>  
          <span className="sans small text-muted">Number of cars that passes the intersection per hour</span>  
        </div>       
        <div className="col-12 mt-4">
          <FlowRateSettings/>
        </div>
      </div>
  </div>
  <div className="container p-3 splitBottom">
      <div className="row">
        <div className="col-12">
          <span className="sans bold">Vehicle Types<br/></span>  
          <span className="sans small text-muted">The Probability of each type of cars to be spawned in the simulation</span>  
        </div>       
        <div className="col-12 mt-4">
          <Vehicles/>
        </div>
      </div>
      <div className="d-flex row mt-4">
        <button id="start" className="simulateButt rounded sans bold">SIMULATE</button>
      </div>
  </div>  
</div>      


    );
  }
}


class App extends React.Component {
  render(){
    return(
      <Settings/>
    )
  }
}


ReactDOM.render(<App/>,document.getElementById('react-ui'));