var envi, setupData, speed = 1, sh = 0, si = 100, envshow, gen = 0, showOption = true, resultFile, b;
var started = false
function preload() {
    setupData = loadJSON("system_config.json");
    //console.log(setupData.lane[0].flowrate)
    resultFile = loadJSON("neural.json");
}
$("#start").click(function () {
    letsGo();
    $("#react-ui").hide()
});

function letsGo() {

    $("#defaultCanvas0").show()
    $("#react-ui").hide()
    
    loop()
    //renderMenus()
    setupData.lane[0].flowrate = parseInt($("#f1").val());
    setupData.lane[1].flowrate = parseInt($("#f1").val());
    setupData.lane[2].flowrate = parseInt($("#f2").val());
    setupData.lane[3].flowrate = parseInt($("#f2").val());
    setupData.lane[4].flowrate = parseInt($("#f3").val());
    setupData.lane[5].flowrate = parseInt($("#f3").val());
    setupData.lane[6].flowrate = parseInt($("#f4").val());
    setupData.lane[7].flowrate = parseInt($("#f4").val());
    console.log(setupData.lane)
    gstop = parseInt($("#gl").val())
    var sumProbs = parseInt($("#sedan").val()) + parseInt($("#cargo").val()) + parseInt($("#truck").val())
    var sedanP = (parseInt($("#sedan").val()) / sumProbs) * 100
    var truckP = (parseInt($("#truck").val()) / sumProbs) * 100
    var cargoP = (parseInt($("#cargo").val()) / sumProbs) * 100
    $("#head").css("background", "transparent")
    $(".logo").removeClass("text-dark")
    $(".logo").addClass("text-white")
    $("#head").removeClass("nice-shadow")
    envi.setting(setupData);
    started = true

}

function setup() {

    var canvas = createCanvas(windowWidth, windowHeight, WEBGL);
    canvas.parent('p5-container')
    noLoop()
    frameRate(30);
    strokeWeight(1);
    envi = new environment();
    b = new neuralnetwork([]);
    b.import(resultFile.brain);
    envi.brain = b;
    camera(0, 0, 400, 0, 0, 0, 0, 1, 0);

}

function draw() {
    background(255);
    if(started){
        for (var i = 0; i < speed; i++) {
            envi.update(4);
            var resultString = envi.steptime.toString();
            for (let ilane of envi.lanelist) resultString = resultString.concat(",", (ilane.countlist[2].number * 10 / (envi.steptime)).toString(), ",", (ilane.countlist[0].number * 10 / (envi.steptime)).toString(), ",");
            for (let ilight of envi.lightlist) resultString = resultString.concat((ilight.status == "red") ? "R" : "G", ",");
            //console.log(resultString);
        }
        envi.show();
    }
}