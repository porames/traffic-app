var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NumberInput = function (_React$Component) {
  _inherits(NumberInput, _React$Component);

  function NumberInput() {
    _classCallCheck(this, NumberInput);

    var _this = _possibleConstructorReturn(this, (NumberInput.__proto__ || Object.getPrototypeOf(NumberInput)).call(this));

    _this.state = {
      value: 20
    };
    _this.handleChange = _this.handleChange.bind(_this);
    _this.componentDidMount = _this.componentDidMount.bind(_this);
    return _this;
  }

  _createClass(NumberInput, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({ value: this.props.defaultVal });
    }
  }, {
    key: "handleChange",
    value: function handleChange(e) {
      this.setState({ value: e.target.value });
    }
  }, {
    key: "render",
    value: function render() {
      var _React$createElement;

      return React.createElement("input", (_React$createElement = { className: "numberValue", onChange: this.handleChange, value: this.state.value, type: "number" }, _defineProperty(_React$createElement, "className", "sans customNumber"), _defineProperty(_React$createElement, "id", this.props.id), _React$createElement));
    }
  }]);

  return NumberInput;
}(React.Component);

var Slider = function (_React$Component2) {
  _inherits(Slider, _React$Component2);

  function Slider() {
    _classCallCheck(this, Slider);

    var _this2 = _possibleConstructorReturn(this, (Slider.__proto__ || Object.getPrototypeOf(Slider)).call(this));

    _this2.state = {};
    _this2.handleChange = _this2.handleChange.bind(_this2);
    _this2.componentDidMount = _this2.componentDidMount.bind(_this2);
    return _this2;
  }

  _createClass(Slider, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setState({ value: this.props.defaultVal });
    }
  }, {
    key: "handleChange",
    value: function handleChange(e) {
      this.setState({ value: e.target.value });
      this.props.value({ value: e.target.value, id: e.target.id });
    }
  }, {
    key: "render",
    value: function render() {
      return React.createElement(
        "div",
        null,
        React.createElement("input", { value: this.state.value, min: "0", max: "10", onChange: this.handleChange, type: "range", className: "custom-range", id: this.props.id })
      );
    }
  }]);

  return Slider;
}(React.Component);

var SettingsRowNumber = function (_React$Component3) {
  _inherits(SettingsRowNumber, _React$Component3);

  function SettingsRowNumber() {
    _classCallCheck(this, SettingsRowNumber);

    return _possibleConstructorReturn(this, (SettingsRowNumber.__proto__ || Object.getPrototypeOf(SettingsRowNumber)).apply(this, arguments));
  }

  _createClass(SettingsRowNumber, [{
    key: "render",
    value: function render() {
      return React.createElement(
        "div",
        { className: "row d-flex" },
        React.createElement(
          "div",
          { className: "six columns d-flex" },
          React.createElement(
            "span",
            { className: "sans" },
            this.props.text
          )
        ),
        React.createElement(
          "div",
          { className: "six columns d-flex" },
          React.createElement(NumberInput, { id: this.props.id })
        )
      );
    }
  }]);

  return SettingsRowNumber;
}(React.Component);

var FlowRateSettings = function (_React$Component4) {
  _inherits(FlowRateSettings, _React$Component4);

  function FlowRateSettings() {
    _classCallCheck(this, FlowRateSettings);

    return _possibleConstructorReturn(this, (FlowRateSettings.__proto__ || Object.getPrototypeOf(FlowRateSettings)).apply(this, arguments));
  }

  _createClass(FlowRateSettings, [{
    key: "render",
    value: function render() {
      return React.createElement(
        "table",
        null,
        React.createElement(
          "tr",
          null,
          React.createElement(
            "td",
            null,
            React.createElement("div", { className: "content" })
          ),
          React.createElement(
            "td",
            { className: "road", style: { borderLeft: 'solid 2px #1a1a1a', borderRight: 'solid 2px #1a1a1a' } },
            React.createElement(
              "div",
              { className: "content" },
              React.createElement(
                "div",
                { style: { margin: 'auto' } },
                React.createElement(NumberInput, { defaultVal: "600", id: "f1" })
              )
            )
          ),
          React.createElement("td", null)
        ),
        React.createElement(
          "tr",
          null,
          React.createElement(
            "td",
            { className: "road", style: { borderTop: 'solid 2px #1a1a1a', borderBottom: 'solid 2px #1a1a1a' } },
            React.createElement(
              "div",
              { className: "content" },
              React.createElement(
                "div",
                { style: { margin: 'auto' } },
                React.createElement(NumberInput, { defaultVal: "600", id: "f2" })
              )
            )
          ),
          React.createElement("td", { className: "road" }),
          React.createElement(
            "td",
            { className: "road", style: { borderTop: 'solid 2px #1a1a1a', borderBottom: 'solid 2px #1a1a1a' } },
            React.createElement(
              "div",
              { className: "content" },
              React.createElement(
                "div",
                { style: { margin: 'auto' } },
                React.createElement(NumberInput, { defaultVal: "600", id: "f3" })
              )
            )
          )
        ),
        React.createElement(
          "tr",
          null,
          React.createElement("td", null),
          React.createElement(
            "td",
            { className: "road", style: { borderLeft: 'solid 2px #1a1a1a', borderRight: 'solid 2px #1a1a1a' } },
            React.createElement(
              "div",
              { className: "content" },
              React.createElement(
                "div",
                { style: { margin: 'auto' } },
                React.createElement(NumberInput, { defaultVal: "600", id: "f4" })
              )
            )
          ),
          React.createElement("td", null)
        )
      );
    }
  }]);

  return FlowRateSettings;
}(React.Component);

var SimulateButton = function (_React$Component5) {
  _inherits(SimulateButton, _React$Component5);

  function SimulateButton() {
    _classCallCheck(this, SimulateButton);

    return _possibleConstructorReturn(this, (SimulateButton.__proto__ || Object.getPrototypeOf(SimulateButton)).apply(this, arguments));
  }

  _createClass(SimulateButton, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      mdc.ripple.MDCRipple.attachTo(document.querySelector('.mdc-button'));
    }
  }, {
    key: "render",
    value: function render() {
      return React.createElement(
        "button",
        { style: { fontFamily: 'Poppins', fontWeight: "600", margin: "auto", marginTop: "1em", fontSize: "13px" }, id: "start",
          className: "mdc-button mdc-button--unelevated" },
        "Simulate"
      );
    }
  }]);

  return SimulateButton;
}(React.Component);

var Vehicles = function (_React$Component6) {
  _inherits(Vehicles, _React$Component6);

  function Vehicles() {
    _classCallCheck(this, Vehicles);

    var _this6 = _possibleConstructorReturn(this, (Vehicles.__proto__ || Object.getPrototypeOf(Vehicles)).call(this));

    _this6.state = {
      sedan: 3,
      truck: 2,
      cargo: 1
    };
    _this6.componentDidMount = _this6.componentDidMount.bind(_this6);
    _this6.updateVal = _this6.updateVal.bind(_this6);
    return _this6;
  }

  _createClass(Vehicles, [{
    key: "componentDidMount",
    value: function componentDidMount() {}
  }, {
    key: "updateVal",
    value: function updateVal(val) {
      this.setState(_defineProperty({}, val.id, val.value));
    }
  }, {
    key: "render",
    value: function render() {
      return React.createElement(
        "div",
        null,
        React.createElement(
          "div",
          { className: "row mb-3" },
          React.createElement(
            "div",
            { className: "col-4" },
            React.createElement(
              "span",
              { className: "sans" },
              "Sedan "
            ),
            React.createElement(
              "span",
              { className: "ml-2 sans bold" },
              this.state.sedan
            )
          ),
          React.createElement(
            "div",
            { className: "col-8" },
            React.createElement(Slider, { defaultVal: this.state.sedan, value: this.updateVal, id: "sedan" })
          )
        ),
        React.createElement(
          "div",
          { className: "row mb-3" },
          React.createElement(
            "div",
            { className: "col-4" },
            React.createElement(
              "span",
              { className: "sans" },
              "Truck "
            ),
            React.createElement(
              "span",
              { className: "ml-2 sans bold" },
              this.state.truck
            )
          ),
          React.createElement(
            "div",
            { className: "col-8" },
            React.createElement(Slider, { defaultVal: this.state.truck, value: this.updateVal, id: "truck" })
          )
        ),
        React.createElement(
          "div",
          { className: "row" },
          React.createElement(
            "div",
            { className: "col-4" },
            React.createElement(
              "span",
              { className: "sans" },
              "Cargo "
            ),
            React.createElement(
              "span",
              { className: "ml-2 sans bold" },
              this.state.cargo
            )
          ),
          React.createElement(
            "div",
            { className: "col-8" },
            React.createElement(Slider, { defaultVal: this.state.cargo, value: this.updateVal, id: "cargo" })
          )
        )
      );
    }
  }]);

  return Vehicles;
}(React.Component);

var Settings = function (_React$Component7) {
  _inherits(Settings, _React$Component7);

  function Settings() {
    _classCallCheck(this, Settings);

    return _possibleConstructorReturn(this, (Settings.__proto__ || Object.getPrototypeOf(Settings)).apply(this, arguments));
  }

  _createClass(Settings, [{
    key: "render",
    value: function render() {
      return React.createElement(
        "div",
        { className: "rounded card-custom pt-3 mb-3" },
        React.createElement(
          "div",
          { className: "option" },
          React.createElement(
            "span",
            { className: "sans bold", style: { margin: 'auto' } },
            "Settings"
          )
        ),
        React.createElement(
          "div",
          { className: "pl-3 pr-3 pb-3 container splitBottom" },
          React.createElement(
            "div",
            { className: "row d-flex mt-3" },
            React.createElement(
              "div",
              { className: "col-12 mb-1" },
              React.createElement(
                "span",
                { className: "sans bold" },
                "Traffic Light"
              )
            ),
            React.createElement(
              "div",
              { className: "col-6" },
              React.createElement(
                "span",
                { className: "sans" },
                "Maximum Cycle Length Allowed (sec)"
              )
            ),
            React.createElement(
              "div",
              { className: "col-6 mb-3" },
              React.createElement(
                "div",
                { className: "d-flex" },
                React.createElement(NumberInput, { defaultVal: "40", id: "gl" })
              )
            )
          )
        ),
        React.createElement(
          "div",
          { className: "container p-3 splitBottom" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-12" },
              React.createElement(
                "span",
                { className: "sans bold" },
                "Flow rate",
                React.createElement("br", null)
              ),
              React.createElement(
                "span",
                { className: "sans small text-muted" },
                "Number of cars that passes the intersection per hour"
              )
            ),
            React.createElement(
              "div",
              { className: "col-12 mt-4" },
              React.createElement(FlowRateSettings, null)
            )
          )
        ),
        React.createElement(
          "div",
          { className: "container p-3 splitBottom" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-12" },
              React.createElement(
                "span",
                { className: "sans bold" },
                "Vehicle Types",
                React.createElement("br", null)
              ),
              React.createElement(
                "span",
                { className: "sans small text-muted" },
                "The Probability of each type of cars to be spawned in the simulation"
              )
            ),
            React.createElement(
              "div",
              { className: "col-12 mt-4" },
              React.createElement(Vehicles, null)
            )
          ),
          React.createElement(
            "div",
            { className: "d-flex row mt-4" },
            React.createElement(
              "button",
              { id: "start", className: "simulateButt rounded sans bold" },
              "SIMULATE"
            )
          )
        )
      );
    }
  }]);

  return Settings;
}(React.Component);

var App = function (_React$Component8) {
  _inherits(App, _React$Component8);

  function App() {
    _classCallCheck(this, App);

    return _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).apply(this, arguments));
  }

  _createClass(App, [{
    key: "render",
    value: function render() {
      return React.createElement(Settings, null);
    }
  }]);

  return App;
}(React.Component);

ReactDOM.render(React.createElement(App, null), document.getElementById('react-ui'));